function TodoList(element) {
    let vm = this;
    let items = [];

    vm.todoElement = document.getElementById(element);
    vm.todoTableHeader = '<thead><tr id="tableHeader"><th class="done">Done</th><th class="name" align="left">Name</th><th align="left" class="priority">Priority</th><th align="center" class="up">Up</th><th align="center" class="down">Down</th><th colspan="4" width="300px">&nbsp;</th></tr></thead>';
    vm.todoPriorityList = ['High', 'Medium', 'Low'];
    vm.selectedItem = '';

    vm.init = function () {
        let elem = vm.todoElement;
        let html = '';

        for (let i = 0; i < vm.todoPriorityList.length; i++) {
            html += '<option value="' + i + '">' + vm.todoPriorityList[i] + '</option>';
        }

        elem.innerHTML = '<form action="#" name="todoForm" id="todoForm">' +
            '<table id="todo-table" cellspacing="0">' +
            '<tr>' +
            '<th>Name</th>' +
            '<td><input type="text" placeholder="Enter Name" id="itemName" maxlength="20" autocomplete="off" /></td>' +
            '<td><select id="itemPriority">' + html + '</select></td>' +
            '<td><button type="submit" class="primary" id="add-save-button"></button></td>' +
            '</tr>' +
            '<tr>' +
            '<td colspan="4"><div><table id="list" cellspacing="0"></table></div></td>' +
            '</tr>' +
            '</table>' +
            '</form>';

        vm.addSaveButton = document.getElementById('add-save-button');
        vm.itemName = document.getElementById('itemName');
        vm.itemPriority = document.getElementById('itemPriority');

        vm.addSaveButton.addEventListener('click', function (e) {
            e.preventDefault();

            let itemName = vm.itemName.value;
            let itemPriority = vm.itemPriority.value;

            if (itemName.trim() !== '') {
                vm.add(itemName, itemPriority);
            } else {
                vm.itemName.className = 'error';
            }
        });
    };

    vm.add = function (name, priority) {
        let obj = {name: name, priority: priority, done: 'false'};

        if (vm.selectedItem !== '') {
            vm.addSaveButton.className = "primary";
            items[vm.selectedItemId] = obj;
        } else {
            items.push(obj);
        }

        vm.display();
    };

    vm.resetItemValues = function () {
        vm.itemName.value = '';
        vm.selectedItem = '';
        vm.itemPriority.value = '0';
        vm.itemName.className = '';
        vm.addSaveButton.className = "primary";
    };

    vm.scrollToBottom = function () {
        let objDiv = document.getElementById("todo-list-content");

        if (objDiv) {
            objDiv.scrollTop = objDiv.scrollHeight;
        }
    };

    vm.getArrayValue = function (val, del, pos) {
        return val.split(del)[pos];
    };

    vm.selectRow = function (idVal) {
        idVal = vm.getArrayValue(idVal, '-', 2);

        let rows = document.getElementsByClassName('rows');

        for (let i = 0; i < rows.length; i++) {
            rows[i].className = 'rows';
        }

        document.getElementById('rows-' + idVal).className = "rows selected";
        vm.selectedItemId = idVal - 1;
        vm.selectedItem = items[vm.selectedItemId];
    };

    vm.edit = function (editId) {
        vm.selectRow(editId);
        vm.itemName.value = vm.selectedItem.name;
        vm.itemPriority.value = vm.selectedItem.priority;
        vm.addSaveButton.className = "success";
    };

    vm.remove = function (deleteId) {
        vm.resetItemValues();
        vm.selectRow(deleteId);

        let selectedItemId = vm.selectedItemId;
        let deleteConfirmation = confirm("Are you sure to delete: '" + items[selectedItemId].name + "'?");

        if (deleteConfirmation) {
            items.splice(selectedItemId, 1);
            document.getElementById('rows-' + parseInt(selectedItemId + 1)).style.display = 'none';
        }
    };

    vm.prioritize = function (id, val) {
        let selectedItemId = vm.getArrayValue(id, '-', 3);

        if (val === 'up' && selectedItemId !== '1') {
            let tempArr = items[selectedItemId - 1];
            items[selectedItemId - 1] = items[selectedItemId - 2];
            items[selectedItemId - 2] = tempArr;
            vm.selectedItem = 'rows-' + parseInt(selectedItemId - 1);
            vm.selectedItemId = parseInt(selectedItemId - 2);
            vm.display();
        } else if (val === 'down' && parseInt(selectedItemId) !== items.length) {
            let tempArr = items[selectedItemId - 1];
            items[selectedItemId - 1] = items[selectedItemId];
            items[selectedItemId] = tempArr;
            vm.selectedItem = 'rows-' + selectedItemId;
            vm.selectedItemId = parseInt(selectedItemId);
            vm.display();
        }
    };

    vm.render = function () {
        vm.init();
        vm.display();
    };

    vm.display = function () {
        let list = '';
        let editRemoveButtons = '';
        let rowId;
        let rowClass;

        for (let i = 1; i <= items.length; i++) {
            rowId = 'rows-' + i;
            rowClass = items[i - 1].done === 'true' ? 'rows done' : 'rows';
            editRemoveButtons = '<td align="center" class="up"><span class="arrows up-arrow-todo" id="up-arrow-todo-' + i + '" title="Move up">⇧</span></td><td align="center" class="down"><span class="arrows down-arrow-todo" title="Move down" id="down-arrow-todo-' + i + '">⇩</span></td><td><button class="info edit-todo" id="edit-todo-' + i + '">Edit</button></td><td><button class="danger remove-todo" id="remove-todo-' + i + '">Delete</button></td>';
            list += '<tr class="rows" id="' + rowId + '"><td align="center" class="done"><input type="checkbox" class="done-todo" id="done-todo-' + i + '" value="' + i + '"></td><td class="name">' + items[i - 1].name + '</td><td class="priority">' + vm.todoPriorityList[items[i - 1].priority] + '</td>' + editRemoveButtons + '</tr>';
        }

        document.getElementById('list').innerHTML = list === '' ? vm.todoTableHeader + list + '<tr class="rows"><td align="center" colspan="7" id="no-item-present">No Item Present</td></tr>' : vm.todoTableHeader + '<tbody id="todo-list-content">' + list + '</tbody>';

        if (items.length > 0) {
            if (vm.selectedItem === '') {
                vm.scrollToBottom();
            } else {
                rowId = 'rows-' + parseInt(vm.selectedItemId + 1);
            }
            vm.todoRowButtonListeners();
            vm.resetItemValues();
            vm.fadeOutFadeIn(rowId);
        }
    };

    vm.fadeOutFadeIn = function (rowId) {
        let rowIdObj = document.getElementById(rowId);

        rowIdObj.className = 'rows fadeOut';

        setTimeout(function () {
            rowIdObj.className = 'rows fadeIn';
        }, 100);
    };

    vm.done = function (id, val) {
        let selectedItemId = vm.getArrayValue(id, '-', 2);
        let rowIdObj = document.getElementById('rows-' + selectedItemId);
        let editIdObj = document.getElementById('edit-todo-' + selectedItemId);
        let removeIdObj = document.getElementById('remove-todo-' + selectedItemId);

        if (val !== '') {
            rowIdObj.className = 'rows done';
            editIdObj.disabled = true;
            removeIdObj.disabled = true;
            items[selectedItemId - 1].done = 'true';
        } else {
            rowIdObj.className = 'rows';
            editIdObj.disabled = false;
            removeIdObj.disabled = false;
            items[selectedItemId - 1].done = 'false';
        }
    };

    vm.todoRowButtonListeners = function () {
        let upArrowButton = document.getElementsByClassName('up-arrow-todo');
        let downArrowButton = document.getElementsByClassName('down-arrow-todo');
        let editButton = document.getElementsByClassName('edit-todo');
        let deleteButton = document.getElementsByClassName('remove-todo');
        let doneButton = document.getElementsByClassName('done-todo');

        for (let i = 0; i < editButton.length; i++) {
            upArrowButton[i].addEventListener('click', function (e) {
                e.preventDefault();
                vm.prioritize(this.id, 'up');
            });

            downArrowButton[i].addEventListener('click', function (e) {
                e.preventDefault();
                vm.prioritize(this.id, 'down');
            });

            editButton[i].addEventListener('click', function (e) {
                e.preventDefault();
                vm.edit(this.id);
            });

            deleteButton[i].addEventListener('click', function (e) {
                e.preventDefault();
                vm.remove(this.id);
            });

            doneButton[i].addEventListener('click', function () {
                let doneId = this.id;
                let val = document.getElementById(doneId).checked ? this.value : '';
                vm.done(this.id, val);
            });
        }
    };

    return vm;
}

let todoList = new TodoList('todo-list');
todoList.render();